package br.com.mastertech.Cartao.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE")
public interface ClienteClient {

    @GetMapping("cliente/{id}")
    Cliente getClienteById(@PathVariable int id);
}
