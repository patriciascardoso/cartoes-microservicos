package br.com.mastertech.Cartao.controller;

import br.com.mastertech.Cartao.DTOs.CartaoCriadoDTO;
import br.com.mastertech.Cartao.DTOs.CartaoDTO;
import br.com.mastertech.Cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoCriadoDTO cadastrarCartao(@RequestBody @Valid CartaoDTO cartaoDTO){
        try {
            return cartaoService.criarCartao(cartaoDTO);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
