package br.com.mastertech.Cartao.services;

import br.com.mastertech.Cartao.DTOs.AtivarCartaoDTO;
import br.com.mastertech.Cartao.DTOs.CartaoCriadoDTO;
import br.com.mastertech.Cartao.DTOs.CartaoDTO;
import br.com.mastertech.Cartao.DTOs.ConsultaCartaoDTO;
import br.com.mastertech.Cartao.client.Cliente;
import br.com.mastertech.Cartao.client.ClienteClient;
import br.com.mastertech.Cartao.models.Cartao;
import br.com.mastertech.Cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public CartaoCriadoDTO criarCartao(CartaoDTO cartaoDTO){
        Cartao cartao = new Cartao();
        Cliente cliente = clienteClient.getClienteById(cartaoDTO.getClienteId());

        cartao.setNumero(cartaoDTO.getNumero());
        cartao.setCliente(cliente.getId());

        cartaoRepository.save(cartao);

        CartaoCriadoDTO cartaoCriadoDTO = new CartaoCriadoDTO();
        cartaoCriadoDTO.setId(cartao.getId());
        cartaoCriadoDTO.setNumero(cartao.getNumero());
        cartaoCriadoDTO.setAtivo(cartao.getAtivo());
        cartaoCriadoDTO.setClienteId(cartao.getCliente());

        return cartaoCriadoDTO;
    }

    private Cartao buscarCartao(int numeroCartao){
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumero(numeroCartao);

        if(optionalCartao.isPresent()){
            return optionalCartao.get();
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public CartaoCriadoDTO alteraStatusCartao(int numeroCartao, AtivarCartaoDTO cartao){
        Cartao cartaoDB = buscarCartao(numeroCartao);
        cartaoDB.setAtivo(cartao.getAtivo());


        Cartao cartaoAlterado = cartaoRepository.save(cartaoDB);
        CartaoCriadoDTO cartaoCriadoDTO = new CartaoCriadoDTO();
        cartaoCriadoDTO.setId(cartaoAlterado.getId());
        cartaoCriadoDTO.setNumero(cartaoAlterado.getNumero());
        cartaoCriadoDTO.setAtivo(cartaoAlterado.getAtivo());
        cartaoCriadoDTO.setClienteId(cartaoAlterado.getId());

        return cartaoCriadoDTO;
    }

    public ConsultaCartaoDTO consultarCartao(int numeroCartao){
        Cartao cartao = this.buscarCartao(numeroCartao);
        ConsultaCartaoDTO consultaCartaoDTO = new ConsultaCartaoDTO();
        consultaCartaoDTO.setId(cartao.getId());
        consultaCartaoDTO.setNumero(cartao.getNumero());
        consultaCartaoDTO.setClienteId(cartao.getId());

        return consultaCartaoDTO;
    }

    public Cartao buscarCartaoPorId(int id){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);

        if(optionalCartao.isPresent()){
            return optionalCartao.get();
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }
}
